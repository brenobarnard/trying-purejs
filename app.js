
var resultsListNode = document.querySelector('#results');
var resultsData = new Array();

function fetchData() {
    for (post of resultsData) {

        // Creating nodes
        let postCard = document.createElement("li");
        postCard.classList.add("post-card");
        
        // Generating Dynamic DOM List
        postCard.innerHTML = ''+
            '<span class="post-id">' + post.id + '</span>' +
            '<span class="user-id">' + post.userId + '</span>' +
            '<h4 class="post-title">' + post.title + '</h4>' +
            '<p class="post-body">' + post.body + '</p>';

        resultsListNode.appendChild(postCard);
    }
}

function requestData(url) {

    // Making a HTML Request using XMLHttpRequest
    let httpResponse = new XMLHttpRequest();

    // Setting callback when request is over
    httpResponse.onreadystatechange = () => {
        if (httpResponse.readyState === 4 && httpResponse.status === 200) {
            resultsData = JSON.parse(httpResponse.response);

            fetchData();
        }
    };

    httpResponse.open("GET", url, true);
    httpResponse.send(null);
}

requestData('https://bitbucket.org/brenobarnard/trying-purejs/raw/02a5a83c6eb6d104a08131ee147d824b8c4af3a2/serverData/dummy.json');